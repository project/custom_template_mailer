<?php

namespace Drupal\custom_template_mailer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form of set email for mailer.
 *
 * @package Drupal\custom_template_mailer
 */
class MailSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'custom_template_mailer.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_template_mailer_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('custom_template_mailer.settings');
    $form['email_addresses'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email Addresses'),
      '#default_value' => $config->get('email_addresses'),
      '#description' => $this->t('Enter the email addresses that should receive updates, one per line.'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the configuration.
    $this->config('custom_template_mailer.settings')
      ->set('email_addresses', $form_state->getValue('email_addresses'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
